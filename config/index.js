'use strict'

module.exports = {
  hostname: 'localhost',
  port: 3000,
  db: {
    host: 'localhost',
    port: 5432,
    database: 'amazingPhones',
    user: 'lifeuser',
  }
}