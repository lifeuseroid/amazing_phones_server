'use strict'

const express = require('express')
const phoneRouter = express.Router()
const app = require('../server');

const {makeResponse} = require('../utils')
const phoneProc = require('../processors').phone

const errors = require('../errors')

phoneRouter.get('/', app.oauth.authenticate(), async (req, res) => {
  const phones = await phoneProc.getAllPhones()
  res.send(makeResponse({data: phones}))
})

phoneRouter.get('/check/:number', async (req, res) => {
  const {isPresent} = await phoneProc.isPresentInDb(req.params.number)
  res.send(makeResponse({data: {isPresent}}))
})

phoneRouter.post('/', app.oauth.authenticate(), async (req, res) => {
  let {number} = req.body
  if (!number) throw errors.fieldFail({number: 'should be present'})

  const phone = await phoneProc.add(number)
  res.send(makeResponse({data: phone}))
})

phoneRouter.delete('/:number', app.oauth.authenticate(), async (req, res) => {
  await phoneProc.deleteNumber(req.params.number)
  res.send(makeResponse())
})


module.exports = phoneRouter