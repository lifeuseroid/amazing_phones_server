'use strict'

const express = require('express')
const authRouter = express.Router()
const app = require('../server');
const oauthModel = require('../oauth/model')
const {makeResponse} = require('../utils')

authRouter.post('/login', app.oauth.token())

authRouter.get('/logout', app.oauth.authenticate(), async (req, res) => {
  await oauthModel.deleteAccessToken(req.query.access_token)
  res.send(makeResponse())
})

module.exports = authRouter