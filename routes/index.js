'use strict'

const express = require('express')
const router = express.Router()

router.use('/auth', require('./auth'))
router.use('/phone', require('./phone'))

module.exports = router