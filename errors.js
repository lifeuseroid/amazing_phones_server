const _ = require('lodash')

const {hostname} = require('./constants')
const prodHostname = 'amazing_phones'

const states = {
  badRequest: 400, unauthorized: 401, forbidden: 403,
  notFound: 404, internalServerError: 500, fail: 418
}

const types = {error: 'error', fail: 'fail'}

const errors = {
  UNAUTHORIZED: {
    code: 'UNAUTHORIZED',
    type: types.error,
    status: states.unauthorized
  },

  // unexpected server error 
  INTERNAL_SERVER_ERROR: {
    code: 'INTERNAL_SERVER_ERROR',
    type: types.error,
    status: states.internalServerError,
  },

  // general expected server error
  FAIL: {
    code: 'FAIL',
    type: types.fail,
    status: states.fail
  },

  // failed on specific request fields
  FIELD_FAIL: {
    code: 'FIELD_FAIL',
    type: types.fail,
    status: states.fail
  }
}

const errCodes = exports.errCodes = {}
for (let key in errors) { errCodes[errors[key].code] = errors[key].code }

function errByErrCode(errCode, info) {
  const errConfig = errors[errCode]

  const err = new Error(info.msg || errConfig.code)
  err.error =  info? Object.assign(errConfig, {info: info}) : errConfig
  return err
}

exports.internalServerError = (error) => {
  const paramObject = _.isError(error)
    ? {info: {msg: error.message}}
    : {info: {msg: 'error is not an instance of Error'}}

  if (hostname !== prodHostname) {
    if (_.isError(error)) paramObject.info.stack = error.stack
    else paramObject.info.error = error
  }

  return errByErrCode(errors.INTERNAL_SERVER_ERROR.code, paramObject)
}

exports.unauthorized = (info) => errByErrCode(errors.UNAUTHORIZED.code, {info})
exports.fail = ({msg, info}) => errByErrCode(errors.FAIL.code, {msg, info})
exports.fieldFail = (fields) => errByErrCode(errors.FIELD_FAIL.code, {info: {fields}})
