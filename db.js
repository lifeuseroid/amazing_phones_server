'use strict'

const {dbLogger} = require('./loggers')
const _ = require('lodash')

const pgOptions = {
  error: function (err, e) {
    let log = {}
    if (e.cn) log.connection = e.cn
    if (e.query) log.query = e.query
    if (e.params) log.params = e.params
    if (e.ctx) log.ctx = e.ctx 
  }
}

const pgp = require('pg-promise')(pgOptions)
pgp.pg.defaults.parseInt8 = true


const config = require('./config').db
const db = pgp(config)

const connParamsForLog = _.pick(config, ['host', 'port', 'database', 'user', 'ssl'])

db.connect()
  .then((result) => {
    dbLogger.info(connParamsForLog)
    result.done()
  })
  .catch((err) => {
    dbLogger.fatal(Object.assign({err: err}, connParamsForLog))
    process.exit()
  })

exports.db = db
exports.pgp = pgp