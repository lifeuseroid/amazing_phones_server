'use strict'

const _ = require('lodash')
const dbTables = require('./tables')

exports.checkEntityName = function checkEntityName(name) {
  if (!dbTables[name])
    throw new Error(`DbEntity can't be used without entityName`)
}

exports.checkColumnSet = function checkColumnSet(columnSets, csName) {
  if (!columnSets[csName])
    throw new Error(`${columnSets} ${csName} columnSet is not found or has wrong type!`)
}

const validateInt = exports.validateInt = function validateInt(int) {
  if (!(Number.isInteger(int) && int > 0))
    throw new Error(`${int} should be positive integer > 0`)
}

const validateString = exports.validateString = function validateString(str) {
  if (!_.isString(str))
    throw new Error(`${str} should be String`)
}

const validateObject = exports.validateObject = function validateObject(data) {
  if (!_.isObject(data) || _.isArray(data))
    throw new Error(`${data} should be Object`)
}

const validateCollection = exports.validateCollection = function validateCollection(collection) {
  if (!Array.isArray(collection)) throw new Error(`${collection} should be Array`)
    for (let obj of collection) { validateObject(obj) }
}

const validateIdsArray = exports.validateIdsArray = function validateIdsArray(ids, idValidator) {
  if (!Array.isArray(ids)) throw new Error(`${ids} should be Array`)
  for (let id of ids) { idValidator(id) }
}
