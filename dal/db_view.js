'use strict'

const {db} = require('../db')

const {checkEntityName, validateInt} = require('./dal_utils')

class DbView {
  constructor(entityName) {
    this.entityName = entityName
  }

  _validateId(...params) {return validateInt(...params)}

  // single
  async getById(id, dbTask =db) {
    this._validateId(id)
    const query = `SELECT * FROM $(entityName~) WHERE id = $(id)`
    return await dbTask.oneOrNone(query, {id, entityName: this.entityName})
  }
}

exports.DbView = DbView