const dbModule = require('../db.js')
exports.db = dbModule.db

exports.dal = Object.assign(
  require('./entities/')
)