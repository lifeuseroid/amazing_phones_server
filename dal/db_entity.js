'use strict'

const _ = require('lodash')

const dbModule = require('../db.js')
const db = dbModule.db
const pgp = dbModule.pgp

const {DbView} = require('./db_view')

const {
  checkColumnSet,
  validateObject, validateCollection, validateIdsArray
} = require('./dal_utils')

class DbEntity extends DbView {
  constructor(entityName, columnSets) {
    super(entityName)
    checkColumnSet(columnSets, 'insert')
    checkColumnSet(columnSets, 'update')
    this.columnSets = columnSets
  }

  _validateCollection(...params) {return validateCollection(...params)}
  _validateObject(...params) {return validateObject(...params)}
  _validateIdsArray (ids) {return validateIdsArray(ids, this._validateId)}  // _validateId is defined in the dbView class

  async addNew(data, dbTask =db) {
    this._validateObject(data)
    const query = pgp.helpers.insert(data, this.columnSets.insert) + ` RETURNING *`
    return await dbTask.one(query)
  }
  async updateById(data, dbTask =db) {
    this._validateObject(data)
    this._validateId(data.id)
    const query = pgp.helpers.update(data, this.columnSets.update) + ` WHERE id = $(id) RETURNING *`
    return await dbTask.oneOrNone(query, {id: data.id})
  }
  async deleteById(id, dbTask =db) {
    this._validateId(id)
    const query = `DELETE FROM $(entityName~) WHERE id = $(id) RETURNING *`
    return await dbTask.oneOrNone(query, {id, entityName: this.entityName})
  }

  // miltiple
  async addNewMultiple(data, dbTask =db) {
    this._validateCollection(data)
    const query = pgp.helpers.insert(data, this.columnSets.insert) + `RETURNING *`
    return await dbTask.manyOrNone(query)
  }
  async updateMultiple(data, dbTask =db) {
    this._validateCollection(data)
    const query = pgp.helpers.update(data, this.columnSets.update) + ` WHERE v.id = t.id RETURNING *`
    return await dbTask.manyOrNone(query)
  }
  async deleteMultiple(ids, dbTask =db) {
    this._validateIdsArray(ids)
    const query = `DELETE FROM $(entityName~) WHERE id IN ($(ids:csv)) RETURNING *`
    return await dbTask.manyOrNone(query, {ids, entityName: this.entityName})
  }
}

exports.DbEntity = DbEntity