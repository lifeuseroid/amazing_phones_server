'use strict'

const dbModule = require('../../db')
const db = dbModule.db
const {ColumnSet} = dbModule.pgp.helpers

const {DbEntity} = require('../db_entity')
const dbTables = require('../tables')
const {validateString} = require('../dal_utils')

const userTable = {table: dbTables.user}

class User extends DbEntity {
  constructor() {
    const id = 'id',
    email = 'email',
    hpassword = 'hpassword'

    const insert = ColumnSet([email, hpassword], userTable)
    const update = ColumnSet([id, email, hpassword], userTable)

    super(userTable.table, {update, insert})
  }

  async getByEmail(email, dbTask =db) {
    validateString(email)
    const query = `SELECT * FROM $(entityName~) WHERE email = $(email)`
    return await dbTask.oneOrNone(query, {email, entityName: this.entityName})
  }
}

module.exports = new User()