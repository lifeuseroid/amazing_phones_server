'use strict'

const dbModule = require('../../db')
const db = dbModule.db
const {ColumnSet} = dbModule.pgp.helpers

const {DbEntity} = require('../db_entity')
const dbTables = require('../tables')

const tokensTable = {table: dbTables.oauth_tokens}

class OauthToken extends DbEntity {
  constructor() {
    const id = 'id',
    access_token = 'access_token',
    access_token_expires_on = 'access_token_expires_on',
    client_id = 'client_id',
    refresh_token = 'refresh_token',
    refresh_token_expires_on = 'refresh_token_expires_on',
    user_id = 'user_id'

    const insert = ColumnSet([
      id, access_token, access_token_expires_on, client_id,
      refresh_token, refresh_token_expires_on, user_id
    ], tokensTable)
    const update = insert

    super(tokensTable.table, {update, insert})
  }

  async getByAccessToken(accessToken, dbTask =db) {
    const query = 'SELECT * FROM $(entityName~) WHERE access_token = $(accessToken)'
    return await dbTask.oneOrNone(query, {
      accessToken,
      entityName: this.entityName
    })
  }

  async deleteByAccessToken(accessToken, dbTask =db) {
    const query = 'DELETE FROM $(entityName~) WHERE access_token = $(accessToken) RETURNING *'
    await dbTask.one(query, {
      accessToken,
      entityName: this.entityName
    })
  }
}

module.exports = new OauthToken()