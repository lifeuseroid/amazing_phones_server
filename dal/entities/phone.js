'use strict'

const dbModule = require('../../db')
const db = dbModule.db
const {ColumnSet} = dbModule.pgp.helpers

const {DbEntity} = require('../db_entity')
const dbTables = require('../tables')

const userTable = {table: dbTables.phone}

class Phone extends DbEntity {
  constructor() {
    const id = 'id',
    number = 'number'

    const insert = ColumnSet([number], userTable)
    const update = ColumnSet([id, number], userTable)

    super(userTable.table, {update, insert})
  }

  async getAll(dbTask = db) {
    const query = `SELECT * FROM $(entityName~)`
    return await dbTask.many(query, {entityName: this.entityName})
  }

  async getByNumber(number, dbTask = db) {
    const query = `SELECT * FROM  $(entityName~) WHERE number = $(number)`
    return await dbTask.oneOrNone(query, {entityName: this.entityName, number})
  }
}

module.exports = new Phone()