'use strict'

const dbModule = require('../../db')
const db = dbModule.db
const {ColumnSet} = dbModule.pgp.helpers

const {DbEntity} = require('../db_entity')
const dbTables = require('../tables')

const clientsTable = {table: dbTables.oauth_clients}

class OauthClient extends DbEntity {
  constructor() {
    const clientId = 'client_id',
    clientSecret = 'client_secret',
    redirectUri = 'redirect_uri'

    const insert = ColumnSet([clientId, clientSecret, redirectUri], clientsTable)
    const update = ColumnSet([clientId, clientSecret, redirectUri], clientsTable)

    super(clientsTable.table, {update, insert})
  }

  async getByClientIdAndSecret(clientId, clientSecret, dbTask =db) {
    const query = 'SELECT * FROM $(entityName~) WHERE client_id = $(clientId) and client_secret = $(clientSecret)'
    return await dbTask.oneOrNone(query, {
      clientId,
      clientSecret,
      entityName: this.entityName
    })
  }
}

module.exports = new OauthClient()