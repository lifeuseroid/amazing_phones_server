module.exports = {
  user: require('./user'),
  phone: require('./phone'),
  oauthClients: require('./oauth_clients'),
  oauthTokens: require('./oauth_tokens'),
}