'use strict'

const {systemLogger} = require('./loggers')

const  {port, db, hostname} = require('./config')
if (!port || !hostname || !db) {
  systemLogger.error('Check config file')
  process.exit(-1)
}

exports.port = port
exports.db = db
exports.hostname = hostname