'use strict'

const {dal, db} = require('../dal')

const uuidv4 = require('uuid/v4')

exports.getClient = async function getClient(clientId, clientSecret) {
  const client = await dal.oauthClients.getByClientIdAndSecret(clientId, clientSecret)

  if (!client) return

  return {
    clientId: client.client_id,
    clientSecret: client.client_secret,
    grants: ['password']
  }
}

exports.getUser = async function getUser(username, password) {
  const user = await dal.user.getByEmail(username)
  if (!user) return false
  if (user.hpassword !== password) return false
  return user
}


exports.saveToken = async function(token, client, user) {
  const tokenInDb = await dal.oauthTokens.addNew({
    id: uuidv4(),
    access_token: token.accessToken,
    access_token_expires_on: token.accessTokenExpiresAt,
    client_id: client.clientId,
    refresh_token: token.refreshToken,
    refresh_token_expires_on: token.refreshTokenExpiresAt,
    user_id: user.id
  })

  if (!tokenInDb) return false
  return Object.assign({client: {id: client.clientId}}, {user: {id: user.id}}, token)
}


exports.getAccessToken = async function getAccessToken(bearerToken) {
  const token = await dal.oauthTokens.getByAccessToken(bearerToken)
  if (!token) return false
  return {
    accessToken: token.access_token,
    client: { id: token.client_id },
    accessTokenExpiresAt: new Date(token.access_token_expires_on),
    user: { id: token.user_id },
  }
}

exports.deleteAccessToken = async function deleteAccessToken(bearerToken) {
  await dal.oauthTokens.deleteByAccessToken(bearerToken)
}
