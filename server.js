'use strict'

require('./db')

const {systemLogger} = require('./loggers')
const {port} = require('./constants')
const errors = require('./errors')
const {makeResponse} = require('./utils')

const _ = require('lodash')
const express = require('express')
const oauthServer = require('express-oauth-server');
require('express-yields')
const cors = require('cors')
const bodyParser = require('body-parser')
const app = module.exports = express();
app.oauth = new oauthServer({
  model: require('./oauth/model'),
  useErrorHandler: true,
  allowBearerTokensInQueryString: true
})
systemLogger.info('Express started')

app.use(cors())
app.use(require('express-bunyan-logger')())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }));


app.use('/api', require('./routes'))
app.post('/oauth/v2/token', app.oauth.token())

app.use((err, req, res, next) => {
  if (err) {
    systemLogger.error(err)
    if (!err.error || !err.error.code || !errors.errCodes[err.error.code])
      err = errors.internalServerError(err)

    res.status(err.error.status).send(makeResponse({
      status: err.error.type,
      data: _.pick(err.error, ['code', 'info'])
    }))
  }
})

app.listen(port, () => systemLogger.info(`server started at port ${port}`))