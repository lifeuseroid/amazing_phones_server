'use strict'

const _ = require('lodash')

exports.makeResponse = ({status ='sucsess', data} = {}) => {
  let res = {status: status}
  if (data) res.data = _.cloneDeep(data)
  return res
}