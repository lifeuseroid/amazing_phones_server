'use strict'

const {systemLogger} = require('./loggers')

async function start() {
  try {
    require('./server.js')
    systemLogger.info('App started')
  }
  catch(e) {
    systemLogger.error(e.toString(), e.stack)
  }
}

start()

process.on('uncaughtException', (e) => { systemLogger.error('uncaughtException', e.toString()) })
process.on('unhandledRejection', (e) => { systemLogger.error('uncaughtException', e.toString()) })