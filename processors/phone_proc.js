'use strict'

const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance()

const errors = require('../errors')

const {dal, db} = require('../dal')

exports.getAllPhones = async function getAllPhones() {
  return await dal.phone.getAll();
}

const isPresentInDb = exports.isPresentInDb = async function isPresentInDb(number) {
  const normalizedNumber = normalizePhone(number)
  await checkCorrectness(normalizedNumber)
  const phone = await dal.phone.getByNumber(normalizedNumber)
  return {isPresent: !!phone, normalizedNumber, phone}
}

exports.add = async function add(number) {
  return await db.task(async function (dbTask) {
    const {isPresent, normalizedNumber} = await isPresentInDb(number, dbTask)
    if (isPresent)
      throw errors.fail({msg: 'Phone already exist!', info: normalizedNumber})
    
    const phone = await dal.phone.addNew({number: normalizedNumber}, dbTask)
    return phone
  })
}

exports.deleteNumber = async function deleteNumber(number) {
  return await db.task(async function (dbTask) {
    const {phone, normalizedNumber} = await isPresentInDb(number, dbTask)
    if (!phone)
      throw errors.fail({msg: 'Phone doesnt exist!', info: normalizedNumber})
    
    await dal.phone.deleteById(phone.id, dbTask)
  })
}

function normalizePhone(number) {
	if (!number) return null
  number = number.replace(/[^0-9]/g, '')

	if (number.length === 11 && number[0] == '8')
    number = '7' + number.slice(1)

	return number;
}

function checkCorrectness(number) {
  const validationError = errors.fail({msg: 'INVALID_PHONE', info: number})
  if (!number) throw validationError

	// for libphonenumber phone must be a string
	let phoneStr = number.toString()
	// to succesfully parse phone number it must start with + sign
	if (phoneStr[0] !== '+') phoneStr = '+' + phoneStr

	let phoneNum,
    verdict = false

	try {
		phoneNum = phoneUtil.parse(phoneStr)
		verdict = phoneUtil.isValidNumber(phoneNum)
	}
  catch (e) { throw validationError }

  if (!verdict) throw validationError
}