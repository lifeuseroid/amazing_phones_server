'use strict'

const bunyan = require('bunyan')

exports.systemLogger = bunyan.createLogger({
  name: 'systemLogger'
})
  
exports.dbLogger = bunyan.createLogger({
  name: 'dbLogger'
})